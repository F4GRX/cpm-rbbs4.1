/****************************************************************
* LED.C

Release 9: RBBS 4.1 Edit 02 - Added support for RC2014 7-segment LED						        *

* This file contains the functions:

* clr_leds	clear (turn off) all segments

* clr_dots	clear (turn off) all dots

* clr_led	clear (turn off) a single segment

* clr_dot	clear (turn off) a single dot

* set_led	set (turn on) a single segment

* set_dot	set (turn on) a single dot

* led_out	display a 4 digit integer (leading zeroes)
		on the 4x 7-segment LED module

****************************************************************/
#include	<bdscio.h>
#include	"rbbs4.h"

/***************************************************************/
#if LEDMOD
clr_leds(psegs) 
int *psegs;
{
   int count;
   count = 0;

   /* clear all digits */
   do {
      clr_led(count, psegs);
      count++;
   } while (count < 4);
}

clr_dots(pdots) 
int *pdots;
{
   int count;
   count = 0;

   /* clear all dots */
   do {
      clr_dot(count, pdots);
      count++;
   } while (count < 4);
}

clr_led(idx, psegs) 
int idx;
int *psegs;
{
   outp(psegs[idx], 10);
}

clr_dot(idx, pdots) 
int idx;
int *pdots;
{
   outp(pdots[idx], 0);
}

set_led(idx, val, psegs) 
int idx;
int val;
int *psegs;
{
   outp(psegs[idx], val);
}

set_dot(idx, pdots) 
int idx;
int *pdots;
{
   outp(pdots[idx], 1);
}

set_dots(pdots) 
int *pdots;
{
   int count;
   count = 0;

   /* set all dots */
   do {
      set_dot(count, pdots);
      count++;
   } while (count < 4);
}

led_out(num, zro, psg)
int num;
int zro;
int *psg;
{
   int cnt, len;
   char tstr[5];

   cnt = 0;

   if (zro) {
      sprintf(tstr,"%04d", num);

      do {
         set_led((3-cnt), (tstr[cnt] - '0'), psg);
         cnt++;
      } while (cnt < 4);
   } else {
      sprintf(tstr,"%d", num);
      len=strlen(tstr);
      
      do {
         set_led((cnt), (tstr[((len-1)-cnt)] - '0'), psg);
         cnt++;
     } while (cnt < len);
  }
}
#endif
