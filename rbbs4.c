/****************************************************************
* RBBS4.C	 						*

Release 12: RBBS 4.1 Edit 01  - Added getpause() function, moved bulletins(); 
                                call beyond user login.
Release 11: RBBS 4.1 Edit 01  - Added txtfiles() function
			      - Added I command to display TEXT files
Release 10: RBBS 4.1 Edit 02  - Added chat() function
			      - Added Y command to enter CHAT
			      - Workaround for USERS.CCC corruption bug
Release  9: RBBS 4.1 Edit 02  - Added banner() call before login prompt
                              - Added support for RC2014 7-segment LED
Release  7: RBBS 4.1 Edit 02  - Added test for % MFLAG to disallow
			        E and K commands for such users.
Release  6: RBBS 4.1 Edit 01  - Allow "personal" messages everywhere
			      - Added non-display of summary if already
			        seen a msgfile once
			      - Bypass choice list if typeahead
			      - Added N command to simulate entry of "R;#+"
Release  5: RBBS 4.1 Edit 00  - Names now one large field
			      - Activated lstmsg for 1st 7 non-PERSONALs
			      - Inserted PERSONLY conditional
Release  4: RBBS 4.0 Edit 21  - if no PERSONAL msgs waiting, selects GENERAL
Release  1: RBBS 4.0 Edit 18

First C version of RBBS, inspired by RBBS31

Originally designed and written by John C. Gilbert.
Additional coding (mainly RBBSCIO.C) by Frank J. Wancho.

Principal Beta testing and numerous suggestions by Sigi Kluger,
with additional testing and help from David Schmidt and Ron
Fowler and others.

This program was originally designed to support a
restricted-access, multi-user TurboDOS RCP/M system.

Additional bug fixing and adaption to BDS C >= 1.6 was done by
Niels Haedcke as well as adding the new chat() functionality
and support for the RC2014 4x 7-segment LED module by 
Mr GeleC('s Tech.

This source is herewith released into the public domain with the
understanding that bug reports and revisions be submitted to the
following email address: webmaster (at) ufud (dot) org  or leave
a message at RC-BOX BBS: rc2014.ddns.net:2014

(See any DRI license agreement for any other disclaimers that may apply
to this program.)

* Functions included in this file:

* main		calls logon, loguser, select_MSG, and maindispatch

* compuser	called by checkmsgs to check if current msg is
		for this user

* maindispatch	main command dispatcher

* select_MSG	called by main and by maindispatch if F command.
		Offers subject files for selection and calls
                checkmsgs

* checkmsgs	loops thru current message file, calling compuser
                on each entry.

****************************************************************/
#include	<bdscio.h>
#include	"rbbs4.h"

	int	privct;
	int	fflags[15];

main()
{
	int	userfd,msgfd;
	char	rflg;
	struct	_sum	s;
	struct	_user	u;

	setmem(fflags,15,FALSE);
	outstr(VERSION,1);
	belflg = FALSE;
	expert = FALSE;
	rtnok = FALSE;
	poke(0,0xcd);		/* Change JMP at 0 to CALL		*/
	rflg = peek(0x5d);	/* Get first char of command line	*/
	poke(0x5d,0x20);	/* Clear that first char		*/
	if ( (rflg == 'P') && (peek(0x5b) == 'x') )
		rtnok = TRUE;	/* Set flag if a rerun			*/
	poke(0x5b,0x78);	/* Set flag loc for possible return	*/
	maxcalls=0;
#if LEDMOD
        /* initialize digit and dot port numbers */
        initw(digits, "56,57,58,59");
        initw(dots,"60,61,62,63");

	clr_leds(digits);
	clr_dots(dots);
	set_dots(dots);
#endif 

	userfd = openfile(DSK(USERS.CCC));

	get_date();
	strcpy(logdate,sysdate);

	logon(userfd,u);
	
	/* workaround for USERS.CCC corupption
       	   bug. Needs further investigation !! */
	uno=u.recno;

	if (!rtnok)
        {
		bulletins();
		getpause("[Hit a key to continue!]");
        }

	crlf(1);
	sprintf(tmpstr,"Hello again, %s!!",u.nm);
	outstr(tmpstr,2);
	sprintf(tmpstr,"Last logon: %s",u.lastlog);
	outstr(tmpstr,1);
	sprintf(tmpstr,"This logon: %s",logdate);
	outstr(tmpstr,1);
	strcpy(u.lastlog,logdate);

	if (!rtnok)
		strcpy(u.lastlog,logdate);
	else
		strcpy(logdate,u.lastlog);


	
	maxcalls=loguser(u);
#if LEDMOD
	led_out(u.recno, 0, digits);
	set_dot(0, dots);
#endif
	crlf(1);
	setptr(m,lp);
	fileflag = FALSE;
	msgfd = select_MSG(msgfd,1,u,s);	/* Select PERSONAL	*/
#if	!PERSONLY
	if (!privct)				/* if none,		*/
	{
		fileflag = TRUE;
		msgfd = select_MSG(msgfd,2,u,s);/* select GENERAL	*/
	}
#endif
	maindispatch(msgfd,userfd,u,s);
}
/***************************************************************/
compuser(ct,u,s)
int	ct;
struct	_user	*u;
struct	_sum	*s;
{
	capstr(s->tnm);
	if ( !strcmp(u->nm,s->tnm) || (sysop  && !strcmp("SYSOP",s->tnm)) )
	{
		if(!ct)
		{
			outstr("YOU HAVE MAIL:",1);
			crlf(1);
		}
		dispsum(s);
		return ++ct;
	}
	else	return ct;
}
/***************************************************************/
maindispatch(fd,ufd,u,s)
int	fd;
int	ufd;
struct	_user	*u;
struct	_sum	*s;
{
	int	cmd;
	char	cmd2[2];

#ifdef LEDMOD
		clr_dots(dots);
		set_dot(0, dots);
#endif
	if (!expert)
	   qhelp();

	cmd = FALSE;
	while(TRUE)
	{
#ifdef LEDMOD
		clr_dots(dots);
		set_dot(0, dots);
#endif


		/* workaround for USERS.CCC corupption
        	   bug. Needs further investigation !! */
		u->recno=uno;
			
		while(cmd)
		{
			switch(cmd)
			{
				case 'E':
#ifdef LEDMOD
					clr_dot(0, dots);
					set_dot(2, dots);
#endif
					if (*u->ustat == '%')
						break;
					entermsg(fd,ufd,u,s,FALSE);
#ifdef LEDMOD
					clr_dot(2, dots);
					set_dot(0, dots);
#endif
					break;

				case 'R':
#ifdef LEDMOD
					clr_dot(0, dots);
					set_dot(3, dots);
#endif															
					readmsgs(fd,u,s);
#ifdef LEDMOD
					clr_dot(3, dots);
					set_dot(0, dots);
#endif
					break;

				case 'L':
					summsgs(fd,u,s);
					break;

				case 'K':
					if (*u->ustat == '%')
						break;
					killmsg(fd,u,s);
					break;

				case 'G':
					fd = goodbye(fd,ufd,u,s);
					break;

				case 'W':
					welcome();
					break;

				case 'C':
#ifdef LEDMOD
					clr_dot(0, dots);
					set_dot(1, dots);
#endif
					fd = gocpm(fd,ufd,u,s);
#ifdef LEDMOD
					clr_dot(1, dots);
					set_dot(0, dots);
#endif
					break;

#if	!PERSONLY
				case 'S':
					if (close(fd) == ERROR)
						ioerr("Closing MSG");
					fileflag = TRUE;
					fd = select_MSG(fd,0,u,s);
					break;
#endif

				case 'U':
#if	NOUCMD
					if (!sysop)
						break;
#endif
					crlf(1);
					showuser(ufd,u);
					break;

				case 'I':
					belflg = !belflg;
					break;

				case 'X':
					expert = !expert;
					if (!expert)
	   				   qhelp();
					break;

				case 'B':
					bulletins();
					break;

				case 'P':
					chgpasswd(u,ufd);
					break;

				case 'M':
					msg_info(msgct,TRUE);
#if	!PERSONLY
					himsg(u);
#endif
#if	DATETIME
					crlf(1);
					tos();
#endif
					break;
#if DATEFILE && !DATETIME
				case '$':
					if (!sysop)
					{
						outstr("SysOp only!",2);
						break;
					}
       					formrec(tmpstr,SECSIZ);
					date_prompt(tmpstr);
					put_date(tmpstr);
					conv_date(tmpstr);
					strcpy(sysdate, tmpstr);
					break;				
#endif
				case 'N':
#ifdef LEDMOD
					clr_dot(0, dots);
					set_dot(3, dots);
#endif
					strcpy(sav,"#+");	
					readmsgs(fd,u,s);
#ifdef LEDMOD
					clr_dot(3, dots);
					set_dot(0, dots);
#endif
					break;

				case '?':
					help();
					break;

				case 'T':
					txtfiles();
					break;
					
				case 'Y':
					if (sysop)
					{
						outstr("You are the SysOp!",1);
						break;
					}
					chat(u);
					break;					

				default:
					if(sysop)
					{
						crlf(1);
						lstuser();
					}
					break;
			}
			cmd = FALSE;
		}

		strcpy(tmpstr,"COMMAND: ");
		if (!expert)
		{

#if	DATEFILE && !DATETIME
			strcat(tmpstr,"$,");
#endif
			strcat(tmpstr,"C,E,");
#if	!PERSONLY
			strcat(tmpstr,"S,");
#endif
			strcat(tmpstr,"G,K,R,L,N,T,Y,b,m,p,i,");
#if	!NOUCMD
			strcat(tmpstr,"u,");
#endif
			strcat(tmpstr,"w,x (or ? for help): ");




		}
		outstr(tmpstr,4);
		instr("",cmd2,1);
		cmd = toupper(*cmd2);
		crlf(1);
	}
}
/****************************************************************/
select_MSG(msgfd,choice,u,s)
int	msgfd,choice;
struct	_user	*u;
struct	_sum	*s;
{
	int	fd;
	int	n;
	char	input[3];
	char	rec[SECSIZ];
	int	pflag;

	/* workaround for USERS.CCC corupption
           bug. Needs further investigation !! */
	u->recno=uno;

#if	!PERSONLY
	fd = openfile(DSK(SUBJECTS.CCC));
	if (readrec(fd,0,rec,1) == ERROR)
		ioerr("reading subject");
	sscanf(rec,"%d",&n);
	while ( (!choice) || (choice > n) )
	{
		if (!*sav)
		{
			outstr("Subjects are:",2);
			displ_subj(fd,n,rec);
		}
		outstr("File number: ",4);
		instr("",input,3);
		crlf(1);
		choice = atoi(input);
	}
	close(fd);	
	if ( choice > 8)
		cchoice = 0;
	else
		cchoice = choice - 1;
	setmem(cmsgfile,9,0);
	movmem(rec+8*choice,cmsgfile,8);
#else
	cchoice = 0;
	strcpy(cmsgfile,"PERSONAL");
#endif
	sprintf(msgfile,"%s%s%s",DRIVE,capstr(cmsgfile),".MSG");
	msgct = load_cntrl(msgct,s);
	pflag = TRUE;
	if ( fflags[choice])
		pflag = FALSE;
	msg_info(msgct,pflag);
	msgfd = openfile(msgfile);
	personal = !strcmp(cmsgfile,"PERSONAL");
	if ( actmsg && !fflags[choice])
	{
		sprintf(tmpstr,"Checking the %s Messages File...",cmsgfile);
		outstr(tmpstr,3);
		checkmsgs(msgfd,u,s);
	}
	fflags[choice] = TRUE;
#if	!PERSONLY
	himsg(u);
#endif
	/* workaround for USERS.CCC corupption
           bug. Needs further investigation !! */
	u->recno=uno;

	return msgfd;
}
/***************************************************************/
#if	!PERSONLY
himsg(u)
struct	_user	*u;
{
	if (cchoice)
	{
sprintf(tmpstr,"Highest message number seen in this file is %d.",u->lstmsg[cchoice-1]);
		outstr(tmpstr,2);
	}
}
#endif
/***************************************************************/
checkmsgs(fd,u,s)
int	fd;
struct	_user	*u;
struct 	_sum	*s;
{
	int	i;

	privct = 0;
	for ( i = 1; i <= msgct; i++)
	{
		if (mno[i])
		{
			readsum(fd,mndx[i],s);
			privct = compuser(privct,u,s);
		}
	}
	if (privct)
	{
		outstr("Please (R)ead and (K)ill ",3);
		sprintf(tmpstr,"th%s message%s",(privct==1)?"is":"ese",(privct==1)?".":"s.");
		outstr(tmpstr,1);
	}
	else
		outstr("No new mail for you.",1);
}
/***************************************************************/