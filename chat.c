/****************************************************************
* CHAT.C	 						*

Release 10: RBBS 4.1 Edit 01 - Added chat() user to sysop chat
                             - Added page() LED paging function
                             - Added ledrestore() restore LED status	

* This file includes the following functions:

* chat()	Simple user to sysop chat

* page()	Page user by blinking 7 segment LED module (if
		configured)
		
* ledrestore()	Restore 7 segment LED module to default		 

*****************************************************************/
#include <bdscio.h>
#include "rbbs4.h"

/****************************************************************/
/* Chat with SysOp at local console                             */
chat(u)
struct _user *u;
{
	int loop;
	char line[MLINESIZ];

	loop=1;
	crlf(1);

	if (getyn("Enter CHAT with SYSOP"))
	{
#if LEDMOD
		page();
		crlf(1);
#endif	
		outstr("     You are now in CHAT mode, type BYE on empty line to exit.",2);
#if	RULER
	if (!expert)
		outstr(RLRSTR,1);
#endif
		crlf(1);
		do 
		{
			setmem(sav,SECSIZ,0);
			outstr(" C>: ",0);		
			instr("",line,MLINESIZ-1);
			if ( (!strcmp("BYE",line)) && (strlen(line)==3) )
				loop=0;
			crlf(1);
		} while (loop);
#if LEDMOD
		ledrestore(u->recno);
#endif
	}
	return;
}
/****************************************************************/
/* Blink 7 segment LED module to page sysop                     */
#if LEDMOD
page()
{
	int ledcnt;

	outstr("Paging SYSOP, please wait ",3);         

        for (ledcnt = 0; ledcnt < PGCNT; ledcnt++)
        {        
		outstr(".",0);
		clr_leds(digits);
		sleep(PGDLY);
		set_dots(dots);
		led_out(PGVAL,0,digits);
		sleep(PGDLY);
	}		
}
#endif
/****************************************************************/
/* Restore 7 segment LED to display user number */
#if LEDMOD
ledrestore(u)
int u;
{
	clr_leds(digits);
	clr_dots(dots);
	set_dot(0,dots);
	led_out(u,0,digits);
}
#endif
/****************************************************************/