/****************************************************************
* TEXT.C	 						*

Release 11: RBBS 4.1 Edit 01 - 

* This file includes the following functions:
 
*****************************************************************/
#include <bdscio.h>
#include "rbbs4.h"

/****************************************************************/
/* Display text file                                            */
txtfiles()
{
	char input[3];
	int choice;

	if (bufinmsg("TXTFILES"));
	else
	{
		outstr("NO TEXT FILES",2);
		return;
	}

	outstr("File number: ",4);
	instr("",input,3);
	crlf(1);
 	choice = atoi(input);

	if (!choice)
		return;
	else
		sprintf(tmpstr,"TEXT%03d",choice);

	if (bufinmsg(tmpstr));
	else
		outstr("INVALID CHOICE!",2);
	return;
}
/****************************************************************/

