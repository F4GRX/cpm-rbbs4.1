Brief Documentation for Release 11: RBBS 4.1 Edit 01 for CP/M (Z80/8080)

RBBS4 is a new version of the popular public domain program,
written in BDS-C, co-authored by John Gilbert and Frank Wancho.
It is not upward file-compatible with older versions of RBBS, and
requires BDS-C 1.60 to compile.  See RBBS4.C for more info,
disclaimers, etc.  (I have not tried compiling this program
under any previous versions of BDS-C.)


The major topics covered in this doc file are:

A.  FILES PROVIDED	An itemized list with brief description

B.  NOTES		Some short comments

C.  BUILDING RBBS4	How to get started

D.  OTHER NOTES		Miscellaneous notes

E.  TO DO		What's on tap

F.  UNRESOLVED BUGS	Known, somewhat harmless bugs

G.  ACKNOWLEDGEMENTS	To those who helped Beta Test

H.  TRAILER		Parting words


A.  FILES PROVIDED:

(Files below, marked with an asterisk, are the support files that
RBBS4 expects to be in the designated user area and drive as
defined in RBBS4.H.)

RBBS4.H		The commented header file.  Please set the
                DEFINEs in this file for your desired system
                configuration.

RBBS4.C		The "main" program

MAINOP.C	A collection of functions

LOGON.C		Most of the logon functions

ENTER.C		Most of the entermsg functions

RBBSFN.C	Miscellaneous functions, mostly file I/O

RBBSCIO.C	A rather fancy line editor and associated console
		I/O functions using BIOS calls.  May be used as a
                separate package.

LED.C		Support functions for the RC2014 4x 7-segment LED
		module by Mr. Gelee's Tech.

CHAT.C		The USER <-> SYSOP local console chat functions.

TEXT.C		Contains the txtfiles() function.

TDOSFN.CSM	Lock and free record TurboDOS functions in
                assembler. Must be processed with CASM.  This may
                be optionally excluded by setting the LOCKEM
                DEFINE in RBBS4.H to FALSE and removing the
                reference in the l2 command line.

DAYTIM.CSM	TurboDOS, MP/M and CP/M+ date/time function (105)
                in assembler.  Provide an equivalent function if
                you wish to use your clock card, using this as a
                model.  See below.  If you do not have a clock
                card, set the DATETIME DEFINE in RBBS4.H to
                FALSE.

DATE.CCC	A SYSOP-created ASCII file that contains a date
		in the form of MM/DD/YY This file will be read
		and written to when the DATEFILE DEFINE is set
		to TRUE in RBBS4.H

                The daytime rewrite is courtesy of Ron Fowler,
                and the tos function is courtesy of Sigi Kluger.

BULLETIN.CCC	A SYSOP-created ASCII file that is displayed with
                paging when the program starts up.  The B Command
                also displays this file.  If this file does not
                exist, then a "No BULLETINS" message is displayed
                instead.

WELCOME.CCC	A SYSOP-created ASCII file that is displayed with
                paging when a new user successfully registers.
                The W Command also displays this file.  A sample
                file is provided.

HELP.CCC	An ASCII file that is displayed with paging when
                the user types a ?<cr> to the COMMAND: prompt.  A
                sample file provided.

HANGUP.COM	SYSOP may optionally provide this program or
                something equivalent.  RBBS4 will chain to this
                program with the G Command or after three failed
                attempts to enter the password.  If you do not
                provide this program, set the HANGUP DEFINE in
                RBBS4.H to FALSE.

AUTO.COM	SYSOP may optionally provide this program or
                something equivalent.  RBBS4 will chain to this
                program with the C Command.  If you do not
                provide this program, set the CHAINEXIT DEFINE in
                RBBS4.H to FALSE.

UTIL.C		Creates SUBJECTS.CCC and empty subject.MSG files,
                and an initialized USERS.CCC file with the SYSOP
                entry and default initial password of 123456.
                UTIL also permits the addition of new subject
                files, modification of user entries, addition of
                new users, consolidation of subject.MSG and
                USERS.CCC files, and insertion of long text files
                as messages.

DATE.CCC        If the DATEFILE DEFINE is set in rbbs4.h this file
                will be read to get the current date. It must only
                contain a valid date string (e.g. 01/06/22).

SUBJECTS.CCC	Created by UTIL with two entries: PERSONAL and
                GENERAL.  Others may be added with UTIL.

USERS.CCC	Created and initialized by UTIL.  Holds user info.

CALLERS.CCC	Created by RBBS4 when first used.  A sequential
                log of each user's use of the system.

PERSONAL.MSG	Created by a UTIL command.  It is the initial
                message file examined by the user.  Only
                Personal/private messages are allowed in this
                file.

GENERAL.MSG	Created by a UTIL command.  This and other
                "secondary" subject files may be selected by the
                F Command.  Only messages to "ALL" are allowed in
                these secondary files.

EXIT2CPM.CCC	Displayed when the non-expert exits to CP/M via
                the C Command.  Sample file provided.

TWITMSG.CCC    Displayed when a "twit" logs in.

MSGHELP.CCC	Displayed when the user types "?<cr>" on a new
                line when entering message text.

*SELHELP.CCC	Displayed when the user types a "?" to the SELECT
                prompt.

RCIOHLP0.CCC	Displayed when the user types the Help Character
                (^V) in normal mode.

RCIOHLP2.CCC	Displayed when the user types the Help Character
                (^V) in masked mode.


B.  NOTES

1.  One major difference between RBBS4 and older versions of RBBS
    is the addition of a fancy line editor.  Type a ^V to see the
    available commands and experiment.

2.  If you are not running TurboDOS or multi-user mode, skip the
    inclusion of TDOSFN or equivalent in the l2 command line.

3.  If you are using a clock card, use DAYTIM.CSM as a model for
    the clock function.  This function returns in HL the address
    of a null-terminated ASCII string of the form: dd mmm yyyy
    hh:mm:ss. If you do not provide this function, the user will
    be prompted to enter the date when he logs in. Alternatively,
    you may now use the DATEFILE DEFINE to read the data from the
    DATE.CCC file.

4.  In addition to the DATEFILE DEFINE there is now a new SYSOP
    command available: '$' . This allows the SYSOP to set the 
    current date from within RBBS4. The new date will then be 
    written to the DATE.CCC file.

5.  Support for the RC2014 4x 7-segment LED module by Mr. Gelee's
    Tech can be enabled by setting the LEDMOD DEFINE in rbbs4.h .

6.  The local CHAT can be entered by using the 'Y' command at the
    command prompt.

7.  This version of RBBS4 comes with a new Text Files feature that
    can be selected by using the 'I' command at the command prompt.


C.  BUILDING RBBS4

1.  To build RBBS4 (and UTIL), first check the DEFINEs in
    RBBS4.H. Build RBBS4 first, using the suggested SUBMIT file,
    RBBS4.SUB as a guide.  Then build UTIL, also using UTIL.SUB
    as a guide.

2.  Run UTIL first, to create the necessary files.  Then run
    RBBS4 and login as SYSOP with password of 123456.  Use the P
    Command to change your password.  Then enter any initial
    messages.


D.  OTHER NOTES

1.  User can both delete and insert lines in SELECT's Edit mode.
    To delete a line, select it for editing and type ^X, then a
    period and <cr>.  To insert a new line at line n, type a -n
    to the line number prompt.

2.  The order of the arguments in the l2 line IS important.  You
    can generate faulty .COM files with no warning from l2,
    especially if it goes into disk mode!

3.  There is a DEFINE named PREREG that will not allow a new user
    to access the system without already existing in the
    USERS.CCC file. That means the SYSOP must use UTIL to
    register that new user ahead of time.

4.  LASTREAD DEFINE checks for a LASTCALR file and use that
    information to logon the user who has already logged on
    through some other front-end program like SIGNON.  LASTWRITE
    is used when RBBS4 IS the front-end program and is expected
    to update the LASTCALR file with an entry of the current
    user's name to be used by subsequent programs like XMODEM.
    Please check the code in LOGON.C in between those DEFINEs to
    make sure they either read or write the appropriate format
    for your system.

5.  If LASTREAD is TRUE, RBBS4 capitalizes the first and last
    names it finds before testing if the name exists.  If the
    user doesn't exist, he is then passed through the newuser
    function, skipping the password prompt, which is
    automatically set to "123456" in case you wish to revert for
    some reason.  This means that LASTREAD and PREREG are
    definitely mutually exclusive, or else you MUST manually
    preregister each new user you allow through your front-end
    program.  (Of course, you CAN have both LASTREAD and PREREG
    set TRUE.  Then only a select subset of your users whom you
    preregister will be allowed to use RBBS4.)

6.  You can tag selected users to have "sysop privileges" within
    RBBS4 by setting the MF FLAG.

7.  The MF FLAG means:

	!	= "sysop"
	*	= no CP/M access (user is locked into RBBS4)
	%	= same as *, but no Enter or Kill command
	+	= normal user, with CP/M access
	#	= twit (user logs all the way in and then
		  TWITMSG.CCC is displayed and hangs up.)




E.  TO DO:

1.  Implement the auto-kill of seen private messages on exit.

2.  Keep track of highest seen and provide an auto-survey of new
    messages in the non-PERSONAL message files when that file is
    first selected in a session.

3.  Consolidate first and last name fields into one field to
    allow longer names.

4.  Consider implementing CHARIO.C for file handling.


F.  UNRESOLVED BUGS:

1.  Duplicate entries in USERS.CCC (sometimes).

2.  The CALLERS.CCC entries are improperly displayed when there
    are an odd (or even) number of entries.


H.  ACKNOWLEDGEMENTS

Our thanks to Sigi Kluger, Ron Fowler, and Dave Schmidt for
taking the time and energy to help us beta test this program, and
suggest many features and improvements we didn't envision.

Additional bug fixing and adaption to BDS C >= 1.6 was done by
Niels Haedcke as well as adding the new chat() functionality
and support for the RC2014 4x 7-segment LED module by 
Mr Gelee's Tech.


G.  TRAILER

We fully expect those more expert in C to optimize or perhaps
totally revamp our code.  If you do, please try to follow our
style for spacing and indentation to keep it readable.  If your
changes don't amount to an overhaul, just bump the edit number in
all the files.  If you do overhaul it, bump the minor version
number and reset the Edit number to 0.  Start (or prepend your
changes to) a RBBS4.HIS file, pack it up, and upload it to the
SENECA RCP/M at the number below.  If you convert this to some
other C dialect, please acknowledge your source and change the
name to avoid confusion with this version.

Please report any problems immediately to the following email
address: webmaster (at) ufud (dot) org  or leave a message at
RC-BOX BBS: rc2014.ddns.net:2014


